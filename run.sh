#!/bin/usr/env bash
docker service create --detach --name registry --publish 5000:5000 registry:2

echo "testtest" | docker secret create psql.user.root -
docker volume create psql.data

docker-compose build
docker-compose push

docker stack deploy --compose-file docker-compose.yml mystack