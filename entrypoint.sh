set -e
psql -v ON_ERROR_STOP=1 --username "root" <<-EOSQL
  CREATE DATABASE mydb;
  GRANT ALL PRIVILEGES ON DATABASE mydb TO root;
EOSQL