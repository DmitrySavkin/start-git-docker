FROM postgres:12


RUN apt-get update

RUN apt -y install openjdk-11-jre-headless

#RUN ./gradlew

COPY entrypoint.sh /docker-entrypoint-initdb.d/entrypoint.sh