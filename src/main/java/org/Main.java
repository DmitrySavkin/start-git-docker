package org;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Main {

    String url = "jdbc:postgresql://postgres:5432/custom_db";
    String user = "custom_user";
    String password = "custom_pass";

    public static void main(String[] args) {

        Main main = new Main();
        try {
            main.task();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected  void task() throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {

            System.out.println("Java JDBC PostgreSQL Example");
            // When this class first attempts to establish a connection, it automatically loads any JDBC 4.0 drivers found within
            // the class path. Note that your application must manually load any JDBC drivers prior to version 4.0.
//          Class.forName("org.postgresql.Driver");

            System.out.println("Connected to PostgreSQL database!");
            Statement st = connection.createStatement();
            System.out.println("Reading car records...");

            st.executeQuery(insertDataFromFile("db_create.sql"));
            ResultSet rs = st.executeQuery("SELECT * FROM test");
            System.out.println("id      name");
            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2));
            }

        } catch (SQLException ex) {

            ex.printStackTrace();
        }
    }

    protected String insertDataFromFile(String fileName) throws SQLException {
			StringBuilder sBuilder = new StringBuilder();
			try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
	  			BufferedInputStream bReader = new BufferedInputStream(inputStream);
	  			BufferedReader r = new BufferedReader(new InputStreamReader(bReader, StandardCharsets.UTF_8))) {
	  			String line;
	  			while ((line = r.readLine()) != null) {
	  				sBuilder.append(line);
	  				sBuilder.append(System.getProperty("line.separator"));
	  			}
	  		    return sBuilder.toString();  
	  		} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return null;
	 	 
	  }


}
